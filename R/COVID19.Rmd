---
title: "COVID-19"
#author: "Alejandro Verri Kozlowski - averri@fi.uba.ar"
runtime: shiny
output: 
  flexdashboard::flex_dashboard:
    orientation: columns
    theme: bootstrap
    vertical_layout: fill
    source_code: embed
---

```{r setup, eval=TRUE, warning=FALSE,message=FALSE,echo=FALSE, cache=TRUE,include=FALSE}
## ---------------------------------------------------------------
library(flexdashboard)
library(shiny)
library(lubridate) # epiweek()
library(plyr) # arrange(),  mutate(),  summarise()
library(dplyr) # ddply()
library(DT)
library(tidyr)
library(highcharter)
```

```{r global, eval=TRUE, warning=FALSE,message=FALSE,echo=FALSE, cache=TRUE,include=TRUE}

URL_ReportedCases<-"https://data.humdata.org/hxlproxy/data/download/time_series_covid19_confirmed_global_narrow.csv?dest=data_edit&filter01=explode&explode-header-att01=date&explode-value-att01=value&filter02=rename&rename-oldtag02=%23affected%2Bdate&rename-newtag02=%23date&rename-header02=Date&filter03=rename&rename-oldtag03=%23affected%2Bvalue&rename-newtag03=%23affected%2Binfected%2Bvalue%2Bnum&rename-header03=Value&filter04=clean&clean-date-tags04=%23date&filter05=sort&sort-tags05=%23date&sort-reverse05=on&filter06=sort&sort-tags06=%23country%2Bname%2C%23adm1%2Bname&tagger-match-all=on&tagger-default-tag=%23affected%2Blabel&tagger-01-header=province%2Fstate&tagger-01-tag=%23adm1%2Bname&tagger-02-header=country%2Fregion&tagger-02-tag=%23country%2Bname&tagger-03-header=lat&tagger-03-tag=%23geo%2Blat&tagger-04-header=long&tagger-04-tag=%23geo%2Blon&header-row=1&url=https%3A%2F%2Fraw.githubusercontent.com%2FCSSEGISandData%2FCOVID-19%2Fmaster%2Fcsse_covid_19_data%2Fcsse_covid_19_time_series%2Ftime_series_covid19_confirmed_global.csv"

URL_ReportedDeaths<-"https://data.humdata.org/hxlproxy/data/download/time_series_covid19_deaths_global_narrow.csv?dest=data_edit&filter01=explode&explode-header-att01=date&explode-value-att01=value&filter02=rename&rename-oldtag02=%23affected%2Bdate&rename-newtag02=%23date&rename-header02=Date&filter03=rename&rename-oldtag03=%23affected%2Bvalue&rename-newtag03=%23affected%2Binfected%2Bvalue%2Bnum&rename-header03=Value&filter04=clean&clean-date-tags04=%23date&filter05=sort&sort-tags05=%23date&sort-reverse05=on&filter06=sort&sort-tags06=%23country%2Bname%2C%23adm1%2Bname&tagger-match-all=on&tagger-default-tag=%23affected%2Blabel&tagger-01-header=province%2Fstate&tagger-01-tag=%23adm1%2Bname&tagger-02-header=country%2Fregion&tagger-02-tag=%23country%2Bname&tagger-03-header=lat&tagger-03-tag=%23geo%2Blat&tagger-04-header=long&tagger-04-tag=%23geo%2Blon&header-row=1&url=https%3A%2F%2Fraw.githubusercontent.com%2FCSSEGISandData%2FCOVID-19%2Fmaster%2Fcsse_covid_19_data%2Fcsse_covid_19_time_series%2Ftime_series_covid19_deaths_global.csv"

# ColourList<-colors(distinct=TRUE) %>% gsub(pattern="1",replacement="") %>% 
#   gsub(pattern="2",replacement="") %>% gsub(pattern="3",replacement="") %>%
#     gsub(pattern="4",replacement="") %>% gsub(pattern="5",replacement="") %>% 
#   gsub(pattern="6",replacement="") %>% gsub(pattern="7",replacement="") %>%
#     gsub(pattern="8",replacement="") %>% gsub(pattern="9",replacement="") %>%
#   gsub(pattern="0",replacement="") %>% unique

ColourList<-colors(distinct=TRUE)[-which(colors(TRUE)%in%c("white","black","gray"))]

.buildDataset<-function(URL){
  DATA <- read.csv(URL, stringsAsFactors=FALSE,header = FALSE)
  DATA<-DATA[-2,] # Remove 2nd row
  colnames(DATA)<-DATA[1,] #Identify Headers
  DATA<-DATA[-1,] # Remove Identifiers
  DATA <- select(DATA,Date,State="Province/State",Location="Country/Region",Cases=Value)  
  DATA <- mutate(DATA, Date=as.Date(Date), Cases=as.numeric(Cases)) %>% arrange(Date)
  return(DATA)
}

.summariseByLocation<-function(DATA){
  LDATA <- DATA %>% 
    group_by(Date,Location) %>% summarise(Cases=sum(.data$Cases)) %>% # remove Regions/Provinces
    arrange(Date) %>% # Sort By Date
    ddply(.variables=c("Location"),.fun=function(df){
      mutate(df,Day=sapply(1:nrow(df),function(d){
        as.numeric(difftime(df$Date[d],df$Date[1]))})
        ,Week=sapply(1:nrow(df),function(d) {
          week(df$Date[d])-week(df$Date[1])+1}))
    })
  
  return(LDATA)
}

.summariseDaysOfWeek <- function(LDATA){
  LDATA%>% mutate(DaysOnWeek=0) %>%
    ddply(.variables=c("Location","Week"),.fun= function(df){
      mutate(df,DaysOnWeek=sum(df$Day>0))}) %>%
    return
}

.summariseTotalWeeks <- function(WDATA){
  WDATA%>% mutate(TotalWeeks=0) %>%
    ddply(.variables=c("Location"),.fun= function(df){
      mutate(df,TotalWeeks=sum(df$Cases>0))}) %>%
    return
}

.summariseByDay<-function(LDATA){
  DDATA<- LDATA %>%
    filter(.data$Cases>0) %>% arrange(Day) %>%
    ddply(.variables=c("Location"),.fun=function(df) {
      mutate(df,NewCases=sapply(1:nrow(df),function(d){
        if(d>1){(df$Cases[d])-(df$Cases[d-1])} else {df$Cases[1]}}))})
  return(DDATA)
}

.summariseByWeek<-function(DDATA){
  DDATA %>% arrange(Week) %>% 
    group_by(Week,Location,DaysOnWeek) %>%
    summarise(Cases=max(.data$Cases),Date=max(.data$Date)) %>%
    ddply(.variables=c("Location"),.fun=function(df) {
      mutate(df,NewCases=sapply(1:nrow(df),function(w){
        if(w>1){(df$Cases[w])-(df$Cases[w-1])} else {df$Cases[1]}}))})  %>%
    return
}

.buildSSlogisModels<-function(Cases,Day,Date){
  TrainingData<-data.frame(N=Cases,t=Day)#CountryData %>% rename(t=Day) %>% select(Cases,t)
  t0<-max(Day)
  startCases<-max(Cases)
  startDate<-last(Date)
  nls.control(warnOnly = TRUE,maxiter = 100,minFactor = 1/4096)
  tryCatch(
    expr={
      Start<-getInitial(N~SSlogis(t,A,tc,k),data=TrainingData)
      Model<-nls(N~SSlogis(t,A,tc,k),start=Start,data=TrainingData)
      Coefficients<-coef(Model)
      A <- Coefficients["A"] %>% ceiling %>% unname 
      tc <-Coefficients["tc"] %>% ceiling %>% unname
      k <- Coefficients["k"] %>% unname
      tons<-floor(tc-log(A-1)*k)
      tmax<- 2*tc-tons
      output<-data.frame(conv=TRUE,A=A,tc=tc,k=k,tons=tons,t0=t0,tmax=tmax,startCases=startCases,startDate=startDate)
      return(output)
    },
    error=function(e){
      output<-data.frame(conv=FALSE,A=NA,tc=NA,k=NA,tons=NA,t0=t0,tmax=NA,startCases=startCases,startDate=startDate)
      return(output)
    }
  )
}

.buildCountryModels<- function(df,parameter="Cases"){
  if(parameter=="Cases"){
    model<-.buildSSlogisModels(df$Cases,df$Day,df$Date)
  } else if(parameter=="Deaths"){
    model<-.buildSSlogisModels(df$Deaths,df$Day,df$Date)
  } else stop()
  
  summarise(df,
            conv=model$conv,
            A=model$A,
            tc=model$tc,
            k=model$k,
            tons=model$tons,
            t0=model$t0,
            tmax=max(t0,model$tmax),
            startDate=model$startDate,
            maxCases=SSlogis(input=tmax,Asym = A,xmid = tc,scal =k) %>% ceiling
  )
}
```

```{r data, eval=TRUE, warning=FALSE,message=FALSE,echo=FALSE, cache=TRUE,include=TRUE}

DatasetSource<-"Humanitarian Data Exchange (http://data.humdata.org)"
MIN_DEATHS<-10
MIN_CASES<-50
MIN_TOTAL_DEATHS<-50
ReportedCases<-.buildDataset(URL_ReportedCases) %>%
  .summariseByLocation %>%   .summariseDaysOfWeek

ReportedDeaths <-.buildDataset(URL_ReportedDeaths) %>%
  .summariseByLocation %>%   .summariseDaysOfWeek

ReportedCasesByDay<-left_join(
  ReportedCases  %>% select(Location,Day,Cases,Date),
  ReportedDeaths %>% select(Location,Day,Cases,Date) %>% 
    rename(Deaths=Cases)
) %>% 
  group_by(Location) %>% filter(Deaths>MIN_DEATHS) %>%
  mutate(TotalDeaths=max(Deaths),TotalCases=max(Cases),Day=Day-Day[1]+1) %>%
  filter(TotalDeaths>MIN_TOTAL_DEATHS) %>% 
  select(-TotalDeaths,-TotalCases) 

ModelCases<- ReportedCasesByDay %>%  
  group_by(Location) %>%   filter(Cases>MIN_CASES) %>% 
  ddply(.variables = "Location",.fun = function(df){.buildCountryModels(df,parameter="Cases")}) %>% 
  filter(conv==TRUE) %>% select(-conv)%>% as_tibble()

ModelDeaths<- ReportedCasesByDay %>% filter(Location %in% ModelCases$Location) %>%
  group_by(Location) %>%   filter(Deaths>MIN_DEATHS) %>% 
  ddply(.variables = "Location",.fun = function(df){.buildCountryModels(df,parameter="Deaths")}) %>% 
  filter(conv==TRUE) %>% select(-conv)%>% as_tibble()

ModelCases<- ModelCases %>% filter(Location %in% ModelDeaths$Location)

Locations <- ModelCases$Location %>% unique

ReportedCasesByDay<- ReportedCasesByDay%>% mutate(Source="Reported")  %>% 
  filter(Location %in% Locations) %>% as_tibble()


PredictedCasesByDay<-ReportedCasesByDay %>% group_by(Location) %>%  
  ddply(.variables = "Location",.fun = function(df,mC=ModelCases,mD=ModelDeaths){
    mC<- mC %>% filter(Location==df$Location[1]) 
    mD<- mD %>% filter(Location==df$Location[1])
    #t0<-mC$t0
    tmax<-mC$tmax
    newDay<-seq(from=1,to=tmax,by=1)
    newDate<-mC$startDate+(days(newDay))
    newCases<-SSlogis(input=newDay,Asym = mC$A,xmid = mC$tc,scal = mC$k) %>% ceiling
    newDeaths<-SSlogis(input=newDay,Asym = mD$A,xmid = mD$tc,scal = mD$k) %>% ceiling
    return(data.frame(Cases=newCases,Day=newDay,Deaths=newDeaths,Date=newDate))
  })  %>% as_tibble() #%>%   mutate(Source="Predicted")



```


Logistic Model
=======================================================================

Logistic Model {.sidebar data-width=250}
-----------------------------------------------------------------------
```{r}
ColourList<-colors(distinct=TRUE)[-which(colors(TRUE)%in%c("white","black","gray"))]
selectInput('COUNTRY', label= "Location:",choices = Locations, selected = "Argentina")
mC<-eventReactive(input$COUNTRY,{
  model<-ModelCases %>% filter(Location==input$COUNTRY)
})
mD<-eventReactive(input$COUNTRY,{
  model<-ModelDeaths %>% filter(Location==input$COUNTRY)
})

dataR<-eventReactive(input$COUNTRY,{
  ReportedCasesByDay %>% filter(Location==input$COUNTRY)
})

dataP<-eventReactive(input$COUNTRY,{
  PredictedCasesByDay %>% filter(Location==input$COUNTRY)
})

colourChart<-eventReactive(input$COUNTRY,{
  set.seed(which(Locations==input$COUNTRY))
  sample(ColourList,2)
})



# sliderInput('TR',label ="Day:",min=1,  max=120 , value=1, step=1)
# observeEvent(input$COUNTRY,{
#   model<-ModelCases %>% filter(Location==input$COUNTRY)
#   updateSliderInput(session,"TR",min=as.numeric(model$t0),max=as.numeric(model$tmax),value=as.numeric(model$t0),step=1)
# })
```


Column {data-width=600} 
-----------------------------------------------------------------------
### Reported Cases 
```{r}
renderHighchart({
  highchart()%>%
    hc_add_series(name = "Reported", type="column", pointWidth=12,data = dataR(), hcaes(x=Day,y=Cases),color="darkgreen") %>%
    hc_add_series(name = "Projected", type="line", data = dataP(), hcaes(x=Day,y=Cases),color="yellow",dashStyle = "DashDot") %>%
    # hc_add_theme(hc_theme_538()) %>% 
    # hc_add_theme(hc_theme_handdrawn()) %>% 
    hc_add_theme(hc_theme_chalk())%>% 
    hc_plotOptions(
      series  = list(
        marker = list(enabled = FALSE)
      )
    )%>%
    hc_tooltip(valueDecimals = 0,
               pointFormat = "Day: {point.x} <br> Cases: {point.y}") %>%
    hc_subtitle(    text = "Reported Cases") %>%
    hc_title(text = input$COUNTRY) %>% 
    hc_credits(enabled = TRUE,
               text = paste("Source:",DatasetSource),
               style = list(fontSize = "10px"))
})
```

### Reported Deaths
```{r}
renderHighchart({
  highchart()%>%
    hc_add_series(name = "Reported", type="column", pointWidth=12,data = dataR(), hcaes(x=Day,y=Deaths),color="blue") %>%
    hc_add_series(name = "Projected", type="line", data = dataP(), hcaes(x=Day,y=Deaths),color="lightblue",dashStyle = "DashDot") %>%
    # hc_add_theme(hc_theme_538()) %>% 
    # hc_add_theme(hc_theme_handdrawn()) %>% 
    hc_add_theme(hc_theme_chalk())%>% 
    hc_plotOptions(series  = list(marker = list(enabled = FALSE)))%>%
    hc_tooltip(valueDecimals = 0,pointFormat = "Day: {point.x} <br> Deaths: {point.y}") %>%
    hc_subtitle(    text = "Reported Deaths") %>%
    hc_title(text = input$COUNTRY) %>% 
    hc_credits(enabled = TRUE,text = paste("Source:",DatasetSource),style = list(fontSize = "10px"))
})
```

Column {data-width=500} 
-----------------------------------------------------------------------
### Logistic Parameter for Cases
```{r}
renderDT({
  DATA<-ModelCases%>% select(Location,A,tc,k,tmax)
  datatable(style = "bootstrap",data=DATA,selection=c("none"),rownames = FALSE,editable=FALSE,options = list(pageLength = 100)) %>%
    formatRound(columns=c('k'), digits=3)    }
)
```


### Logistic Parameter for Deaths
```{r}
renderDT({
  DATA<-ModelDeaths%>%select(Location,A,tc,k,tmax)
  datatable(style = "bootstrap", data=DATA,selection=c("none"),rownames = FALSE,editable=FALSE,options = list(pageLength = 100)) %>%
      formatRound(columns=c('k'), digits=3)    }
)
```

