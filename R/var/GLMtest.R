library(dplyr) # ddply()
library(ggplot2)
ReportedCases<-read.csv("ReportedCases.csv", stringsAsFactors=FALSE,header = TRUE)%>% select(Location,Day,Week,Cases)
ReportedDeaths<-read.csv("ReportedDeaths.csv", stringsAsFactors=FALSE,header = TRUE)%>% 
  select(Location,Day,Week,Cases) %>% rename(Deaths=Cases)

DATA<-left_join(ReportedCases %>% select(Location,Day,Week,Cases),
                ReportedDeaths %>%select(Location,Day,Week,Cases) %>% rename(Deaths=Cases))%>% 
  filter(Cases>0,Day>0,Location=="Argentina")%>% 
  mutate(Day=as.numeric(Day),Cases=as.numeric(Cases)) %>% mutate(Mortality=Deaths/Cases) %>%
  mutate(Day=Day-Day[1]+1,Week=Week-Week[1]+1)  # Remove offset (set Onset to 1)
#

SS<-getInitial(Cases~SSlogis(Day,alpha,xmid,scale),data=DATA)
ggplot(as.data.frame(SSlogis(DATA$Day,SS["alpha"],SS["xmid"],SS["scale"])))

#LD<-DATA$Day %>% last
#LW<-DATA$Week %>% last

# Approach #1: MechanisTIC
# # Linear Models
# MDL1<-glm(log(Cases)~I(Day^-1)+I(Day^2)+I(Day^-2)+I(Day^3)+I(Day^-3)+I(Day^-4),data=DATA)
# summary(MDL1)
# MDL2<-glm(log(Cases)~I(Week^-1)+I(Week^2)+I(Week^-2)+I(Week^3)+I(Week^-3)+I(Week^-4),data=DATA)
# summary(MDL2)

# SEIAR model
