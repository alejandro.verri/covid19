library(deSolve)


# Assuming the epidemic begins when the logistic function equals one, the duration of the epidemic can be calculated.
tons<-tc-log(A-1)*k
dte<-2*(tc-tons) # Epidemic Duration
tmax<-tons+dte
tp<-seq(to,max(tmax,180),by=1)





SEAIR.model<-function(#Susceptible, Exposed, Asymptomatic, Infected, Recovered
  t=NULL,
  # Time Parameters
  to=0,
  tmax=365,
  # Initial Conditions (State)
  So=1,
  Io=0.005,
  Eo=0.005,
  Ro=0.0001,
  Ao=1-0.005-0.005-0.0001,
  # Parameters
  PX=5, #Average number of people exposed to an infected person each day
  pEI=0.5, #Probability of each exposure becoming an infection}, 
  TE=3, # Duration of Latency Period
  TI=7, # Duration of Infectious Period (symptomatic)
  TA=14, # Duration of Infectious Period (asymptomatic)
  pAI=0.3 # Probability of developing symptoms)  
  ) { 
  SEIAR.ode <- function(t, InitiaConditions, Parameters) {
    with(as.list(c(InitiaConditions, Parameters)), {
      dS <- -PX*pEI*S*(A+I)
      dE<- +PX*pEI*S*(A+I)-E/TE
      dI<- pAI*E/TE-I/TI
      dA<- (1-pAI)*E/TE-A/TA
      dR<- A/TA+I/TI
      return(list(c(dS, dE, dI,dA,dR)))
    })
  }
  
  Parameters<-c(PX,pEI,TE,TI,TA,pAI)
  InitiaConditions<-c(
    S=So,
    E=Eo,
    I=Io, 
    A=Ao,
    R=Ro
  )
  out<-ode(y=InitiaConditions, times=seq(to,tmax,by=1), func=SEIAR.ode,parms=Parameters)
  return(out)
  
}



