library(dygraphs)
Data<-LocalCases%>%filter(Cases>10)
lungDeaths <- cbind(mdeaths, fdeaths)
dygraph(lungDeaths) %>%
  dyRangeSelector() %>%
  dyBarSeries('fdeaths') %>% 
  dyFilledLine('mdeaths')
